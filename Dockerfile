# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#FROM ubuntu:22.04
FROM nvidia/opengl:1.0-glvnd-devel-ubuntu22.04
LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="July 11, 2023"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo


RUN apt-get update && apt-get install -q -y --no-install-recommends \
    wget build-essential gcc git vim \
    dirmngr vim sudo git\
    libosmesa6-dev wget iputils-ping net-tools \
    x11-utils x11-apps terminator xterm \
    lsb-release iproute2 gnupg gnupg2 gnupg1 \
    ca-certificates language-pack-ja-base locales fonts-takao \
    debconf-utils curl  python3-pip wget && \
    pip install -U --no-cache-dir supervisor supervisor_twiddler rosdep && \
    rm -rf /var/lib/apt/lists/*


# Install ROS2
RUN apt update \
  && apt install -y --no-install-recommends \
     curl gnupg2 lsb-release python3-pip vim wget build-essential ca-certificates
RUN curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg \
  && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null
RUN apt update \
#  && apt upgrade \
  && DEBIAN_FRONTEND=noninteractive \
  && apt install -y --no-install-recommends \
     ros-humble-desktop \
  && rm -rf /var/lib/apt/lists/*

RUN apt update \
  && DEBIAN_FRONTEND=noninteractive \
  && apt install -y --no-install-recommends \
     python3-colcon-common-extensions python3-rosdep2 \
  && rm -rf /var/lib/apt/lists/*

# Add user and group
# ARG {VARIABLE} are defined in .env
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD
ARG WORKSPACE_DIR
RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}

# Terminator Config
RUN mkdir -p /home/${USER_NAME}/.config/terminator/
COPY assets/terminator_config /home/${USER_NAME}/.config/terminator/config 
RUN sudo chmod 777 /home/${USER_NAME}/.config/terminator/config
COPY assets/entrypoint.sh /tmp/entrypoint.sh

RUN bash /opt/ros/humble/setup.sh
RUN source /opt/ros/humble/setup.bash && \
    mkdir -p /home/roboworks/ros2_ws/src && cd /home/roboworks/ros2_ws/src && \
    git clone -b ros2 https://github.com/rt-net/sciurus17_ros.git && \
    git clone -b ros2 https://github.com/rt-net/sciurus17_description.git  && \
    rosdep update && sudo apt-get update  && \
    rosdep install -r -y -i --from-paths . && cd /home/roboworks/ros2_ws  && \
    colcon build --symlink-install  && \
    source /home/roboworks/ros2_ws/install/setup.bash


# .bashrc
#RUN echo "# disable to display dbind-WARNING" >> /home/${USER_NAME}/.bashrc
#RUN echo "export NO_AT_BRIDGE=1" >> /home/${USER_NAME}/.bashrc
RUN echo "source /home/roboworks/ros2_ws/install/setup.bash" >>  /home/${USER_NAME}/.bashrc
RUN echo "source /opt/ros/humble/setup.bash" >> /home/${USER_NAME}/.bashrc

WORKDIR ${WORKSPACE_DIR}
ENTRYPOINT ["/tmp/entrypoint.sh"]
#CMD ["/bin/bash"]
CMD ["terminator"]

